package io.tpd.kafkaexample;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.InvalidPartitionsException;
import org.apache.kafka.common.errors.TopicExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@Component
public class TopicsHelper {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TopicsHelper.class);

    @Autowired
    public Map<String, Object> configs;

   /* private void addTopicsIfNeeded(AdminClient adminClient, Collection<NewTopic> topics) {
        if (topics.size() > 0) {
            Map<String, NewTopic> topicNameToTopic = new HashMap<>();
            topics.forEach(t -> topicNameToTopic.compute(t.name(), (k, v) -> t));
            DescribeTopicsResult topicInfo = adminClient
                    .describeTopics(topics.stream()
                            .map(NewTopic::name)
                            .collect(Collectors.toList()));
            List<NewTopic> topicsToAdd = new ArrayList<>();
            Map<String, NewPartitions> topicsToModify = checkPartitions(topicNameToTopic, topicInfo, topicsToAdd);
            if (topicsToAdd.size() > 0) {
                addTopics(adminClient, topicsToAdd);
            }
            if (topicsToModify.size() > 0) {
                modifyTopics(adminClient, topicsToModify);
            }
        }
    }*/

    public void createTopic(String topicName,int numPartitions) {
        Map<String, Object> producerConfigs = (Map<String, Object>) configs.get("producerConfigs");



        try (final AdminClient adminClient = AdminClient.create(producerConfigs)) {
            NewTopic newTopic = null;
            try {
                // Define topic
               // newTopic = new NewTopic(topicName);

                // Create topic, which is async call.
                final CreateTopicsResult createTopicsResult = adminClient.createTopics(Collections.singleton(newTopic));
               // adminClient.createPartitions()
                // Since the call is Async, Lets wait for it to complete.
                createTopicsResult.values().get(topicName).get();
            } catch (InterruptedException | ExecutionException e) {
                if (!(e.getCause() instanceof TopicExistsException)) {
                    if(newTopic!=null) {
                       // newTopic.
                    }
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
    }


    public void addPartitions(String topicName,int numPartitions) {
        Map<String, Object> producerConfigs = (Map<String, Object>) configs.get("producerConfigs");
        try (final AdminClient adminClient = AdminClient.create(producerConfigs)) {
            try {
                // Define topic
                NewPartitions partitions = NewPartitions.increaseTo(numPartitions);
                Map<String, NewPartitions> partitonsTopic = new HashMap<String, NewPartitions>();
                partitonsTopic.put(topicName,partitions);
                // Create topic, which is async call.
                final CreatePartitionsResult createTopicsResult = adminClient.createPartitions(partitonsTopic);

                // Since the call is Async, Lets wait for it to complete.
                createTopicsResult.values().get(topicName).get();
            } catch (InterruptedException | ExecutionException e) {
                if (!(e.getCause() instanceof InvalidPartitionsException)) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
    }




}
