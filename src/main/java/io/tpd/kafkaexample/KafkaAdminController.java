package io.tpd.kafkaexample;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class KafkaAdminController {


    @Autowired
    TopicsHelper topicsHelper;
    @Autowired
    KafkaTemplate<String, Object> template;

    private static final Logger logger =
            LoggerFactory.getLogger(HelloKafkaController.class);

    @GetMapping("/addPartition/{topicName}/{size}")
    public String addPartition(@PathVariable("topicName") final String topicName,@PathVariable("size") final Integer size) throws Exception {

       // AdminClient admin = AdminClient.create(kafkaAdmin.getConfig());
        topicsHelper.createTopic(topicName,size);
        //NewTopic topic =  TopicBuilder.name(topicName).partitions(size).build();
        //System.out.println(topic);
        return "SUCCESS";
    }

    @GetMapping("/modifyPartition/{topicName}/{size}")
    public String modifyPartition(@PathVariable("topicName") final String topicName,@PathVariable("size") final Integer size) throws Exception {

        // AdminClient admin = AdminClient.create(kafkaAdmin.getConfig());
        topicsHelper.addPartitions(topicName,size);
        //NewTopic topic =  TopicBuilder.name(topicName).partitions(size).build();
        //System.out.println(topic);
        return "SUCCESS";
    }

    @GetMapping("/sendMessage/{topicName}/{size}")
    public String sendMessage(@PathVariable("topicName") final String topicName,@PathVariable("size") final Integer size) throws Exception {
        try {
            topicsHelper.addPartitions(topicName, size);
        }catch (Exception e) {
            e.printStackTrace();
        }
      //  template.send(topicName,size-1,"HELLO","HELLO");
        template.send(topicName,"HELLO","HELLO");
        return "SUCCESS";
    }

}
