package io.tpd.kafkaexample;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class KafkaCustomPatitioner implements Partitioner {
    private static final Logger logger =
            LoggerFactory.getLogger(KafkaCustomPatitioner.class);

    @Override
    public void configure(Map<String, ?> configs) {

    }



    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes,
                         Cluster cluster) {

        int partition = 0;
        String userName = (String) key;
        // Find the id of current user based on the username
        Integer userId = getPartition(userName);
        // If the userId not found, default partition is 0
        if (userId != null) {
            partition = userId;

        }
        logger.info("partition key for data sending :: "+partition);
        return partition;
    }

    private int getPartition(String key) {
       return key.hashCode() % 10;
    }

    @Override
    public void close() {

    }

}

